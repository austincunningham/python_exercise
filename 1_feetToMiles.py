#!/usr/bin/python3

# There are 5280 feet in a mile.
# Write a Python statement that calculates and prints
# the number of feet in 13 miles.

feet=(5280 * 13)
print("%1.0f feet in 13 miles" %feet)
