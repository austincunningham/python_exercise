#!/usr/bin/python3

# Write a Python expression that combines the string "Joe Bloggs is 35 years old."
# from the string "Joe Bloggs" and the number 35 and then prints the result
# (Hint: Use the function str to convert the number into a string.)

name = "Joe Bloggs"
age = 35
print(name + " is " + str(age)+" years old")
