#!/usr/bin/python3

# Write a Python statement that calculates and prints the number of seconds
# in 7 hours,21 minutes and 37 seconds.

seconds = (7 * 60 * 60) + (21 * 60) + (36)
print("%1.2f seconds" %seconds)
