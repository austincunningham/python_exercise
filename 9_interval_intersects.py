#!/usr/bin/python3

# Write a Python function interval_intersect that takes parameters a,b,c,and d
# and returns True if the intervals[a,b] and[c,d] intersect and False otherwise.
# While this test may seem tricky, the solution is actually very simple and
# consists of one line of Python code.(You may assume that a≤b and c≤d.)

def interval_intersects(x,y,a,b):
    if (not ((b <= x) or (y <= a))):
        print("true")
    else:
        print("false")

def main():
    a = 1
    b = 6
    x = 5
    y = 65
    interval_intersects(x,y,a,b)

if __name__=='__main__':
    main()
