#!/usr/bin/python3

# Write a Python function is_even that takes as input the parameter number
# (an integer) and returns True if number is even and False if number is odd.
# Hint: Apply the remainder operator to n(i.e.,number % 2) and compare to zero.

def is_even(number):
    if number % 2 == 0:
        print("even")
    else:
        print("odd")

def main():
    print ("Enter a number to test for odd/even")
    number = input(">")
    is_even(number)

if __name__=='__main__':
    main()
