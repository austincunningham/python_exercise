#!/usr/bin/python3

# Write a Python function miles_to_feet that takes a parameter miles and returns
# the number of feet in miles


def Miles_to_Feet(Miles):
    feet = (Miles * 5280)
    print ("%1.0f feet in %1.0f miles" %(feet,Miles))

def main():
    print("Enter number of miles to convert to feet")
    mile = input(">")
    Miles_to_Feet(mile)

if __name__=='__main__':
    main()
